#!/bin/bash
### BEGIN INIT INFO
# Provides:             whatsapi
# Required-Start:       $remote_fs $syslog
# Required-Stop:        $remote_fs $syslog
# Default-Start:        2 3 4 5
# Default-Stop:
# Short-Description:    Whatsapi Service
### END INIT INFO

. /lib/lsb/init-functions
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"

case "$1" in
  start)
    log_daemon_msg "Starting Whatsapi service" "whatsapi" || true
    cd /GitProjects/whatsapi/

    /usr/bin/screen -dmS test -t test /usr/bin/bash whiletest.sh
    
    /usr/bin/screen -dmS whatsapi -t QR /usr/bin/bash whatsapi.sh QR
    /bin/sleep 0.1

    for line in `ls -1 .config/*js | cut -d"/" -f2 | grep -v config | cut -d "." -f1`
    do 
        /usr/bin/screen -S whatsapi -X screen -t "Linea $line" /usr/bin/bash whatsapi.sh $line
        /bin/sleep 0.1    
    done
    ;;
  stop)
    log_daemon_msg "Stoping Whatsapi service" "whatsapi"  || true
    for i in `/bin/ps ax | /bin/grep -e whatsapi.sh | /bin/grep -v grep |/usr/bin/awk '{print $1}'` ; do /bin/kill $i ; done
    /usr/bin/screen -wipe
    ;;
  restart)
    $0 stop
    sleep 1
    $0 start
    ;;
  reload|force-reload)
    $0 stop
    sleep 1
    $0 start
    ;;
  *)
	log_action_msg "Usage: /etc/init.d/whatsapi {start|stop|restart|reload|force-reload}" || true
	exit 1
esac
