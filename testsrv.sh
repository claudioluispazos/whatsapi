FILETMP='/tmp/testsrv.txt';


#miro el load actual
act=`uptime | cut -d"," -f 4 | cut -d " " -f 5`;
mem=`vmstat -s | awk '/used memory/ { printf "%.3f \n", $1/1024/1024 }'`
time=`date +"scale=0; %M%%60" | bc -l`
wspi=`ps -aux | grep "node whatsapi" | wc -l`;
date;
echo "time $time";
echo "load $act";
echo "mem $mem";
echo "waspi $wspi";
aviso="";
avisar=0;
if (( $(echo "$act > 3.2" | bc -l) ))
then
	aviso="$aviso - load alto";
	avisar=1;
fi
if (( $(echo "$mem > 5" | bc -l) ))
then
	aviso="$aviso - memoria zarpada";	
	avisar=1;
fi
if (( $(echo "$time == 0" | bc -l) ))
then
	aviso="$aviso - es tiempo de avisar";	
	avisar=1;
fi
if (( $(echo "$wspi < 7" | bc -l) ))
then
	aviso="$aviso - pocos whatsapis";	
	avisar=1;
fi

if (( $avisar ))
then
	(
	date;
	echo "\n\n";
	echo "Motivo $aviso";
	echo "\n\n";
	uptime -p
	echo "\n\n";
	uptime | cut -d"," -f 3- | cut -c 3-;
	echo "\n\n";
	free -tm | grep Total | sed 's/Total/Mem/'
	echo "\n\n";
	echo "Whatsapis corriendo $wspi";
	)> $FILETMP;

	mensaje=`cat $FILETMP | tr -d '\n' | tr -d '\r'`;

	for para in 5491121861890; 
	do
		data=`node -pe "JSON.stringify({from: 5491121861890, to:$para, message:'$mensaje'});"`;
		curl -X POST localhost:8000/sendMessage -H "Accept: application/json" -H "Content-Type:application/json" --data "$data";
	done;
	
fi;
echo "listo";
exit;

