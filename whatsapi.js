const { Client, Location, List, Buttons, MessageMedia, LocalAuth } =  require('whatsapp-web.js');
const https = require('https');
const http = require('http');
const fs = require('fs');
const urlencode = require('urlencode');
const qrcode = require('qrcode');
const { createCipheriv } = require('crypto');

const whatsappid = process.argv[2];

var lstMensajes = [];
var SESSION_FILE_PATH;
var CONFIG_FILE_PATH;
var sessionCfg;
var config;
var esQR = false;
var haySession=false;
var imgQR = "";
var cantQR = 0;
var lstSending = [];
var g_chats = [];
var initializing = false;

/*
process.on("SIGINT", async () => {
  console.log("(SIGINT) Shutting down...");
  await client.destroy();
  console.log("despues del destroy");
  process.exit(0);    
});
*/

process.on("SIGINT", async () => 
  {
    logear("(SIGINT) Shutting down...");
    await cerrarSession(false);
    process.exit();
  }
);

if (!whatsappid) 
{
  logear("Debe enviar como argumento el whatsappid, o QR para conectar un nuevo dispositivo");
  return;
}

if (whatsappid=='QR') 
{
  esQR=true;
  CONFIG_FILE_PATH = './.config/config.js'; //uso configuracion por defecto
  fs.rmSync('.wwebjs_auth/session-QR',{ recursive: true, force: true });
} 
else 
{
  CONFIG_FILE_PATH = './.config/'+whatsappid+'.js';
  SESSION_FILE_PATH = './.wwebjs_auth/session-'+whatsappid;
  if (fs.existsSync(SESSION_FILE_PATH)) 
  {
    haySession=true;
  }
}
if (fs.existsSync(CONFIG_FILE_PATH)) 
{
  config =  require(CONFIG_FILE_PATH);
} 
else 
{
  logear("No se encontro configuracion para "+whatsappid);
  return;
}

const client = new Client(
  {
    authStrategy: new LocalAuth({clientId: whatsappid }),
      puppeteer: {
         headless: true,
         handleSIGINT: false,
         defaultViewport: null,
         args: [
             "--no-sandbox",
             "--single-process",
             "--disable-setuid-sandbox",
             "--disable-dev-shm-usage",
             "--disable-accelerated-2d-canvas",
             "--no-first-run",
             "--no-zygote",
             "--disable-gpu",
             ]
      },
      authTimeoutMs: 45000,
      qrMaxRetries: 0,
      takeoverOnConflict: true,
      takeoverTimeoutMs: 1000,
      userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
      ffmpegPath: 'ffmpeg',
      bypassCSP: false,
      proxyAuthentication: undefined
  }
);

const server = http.createServer(fnHttpServer);
server.listen(config.httpPort);
logear("Escucho en el puerto ", config.httpPort);

initClient();

async function initClient()
{
  //const client = new Client({session: sessionCfg});
  try 
  {
    if(haySession)
    {
      logear("Intentando restaurar la ultima sesion");
      sendWebHook({event: 'change_state', content: 'CONNECTING'}, async () =>
        {
          try {
            await client.initialize();
            logear("cliente conectado");
          }
          catch(e)
          {
            logear("NO SE PUDO INICIALIZAR EL CLIENTE");
            logear(e);
            await cerrarSession(true);
            process.exit();
          }
        }
      );
    }
    else
    {
      logear("Espero a que me pidan un QR");
    }
  }
  catch(e)
  {
    logear("NO SE PUDO INICIALIZAR EL CLIENTE");
    logear(e);
    await cerrarSession(true);
    process.exit();
  }  
}

/***************** DE ACA PARA ABAJO LAS FUNCIONES **********************/
function fnHttpServer (req, res) 
{
  try 
  {
    if( req.method == 'GET') 
    {
      try 
      {
        logear(req.url);
        var url = req.url.split("/");
        logear(url);
        switch (url[1]) 
        {
          case "getQR":
            if(esQR) 
            { //en realidad solo soy el proxy
              var custConfig = require('./.config/'+url[2]+'.js');
              logear("redirect =>", custConfig.httpPort)
              sendGet(http, 'localhost', custConfig.httpPort, req.url, (ret) => {
                res.end(ret);
              });
            } 
            else 
            {
              res.setHeader('Access-Control-Allow-Origin', '*');
              res.writeHead(200, { 'Content-Type': 'text/html' });
              if (haySession)
              {
                res.end("Ya hay una session activa");                    
              } 
              else 
              {
                var body = '';
                var html  = '';
                if (cantQR<0) 
                {
                  html = 'Recarge la pagina para generar otro codigo';
                  cantQR=0;
                } 
                else 
                {
                  if (imgQR!='')
                  {
                    body = '<img src="'+imgQR+'"/>';
                  } 
                  else 
                  {
                    if (!initializing)
                    {  //esto deberia ser por cada sesion de cliente. pero bueno. hayue pensarlo, y por ahora no se van a cruzar tantos
                      client.initialize();
                      initializing=true;
                      cantQR=0;
                    }
                    body = 'Estamos generarando un nuevo QR. Aguarde.';
                  }
                  html  = '<html><head><meta http-equiv="refresh" content="3"></head><body>'+body+'</body></html>';
                }
                res.write(html);
                res.end();
              }
            }
            break;
          default:
            res.writeHead(404);
            res.end(JSON.stringify({ res:'Not Found' }) );
        }
      } 
      catch (e) 
      {
        logear(e);
        res.writeHead(500);
        res.end(JSON.stringify({ res:'ERR' }) );
      }
    }
    else if( req.method == 'POST' && esQR ) 
    {
      //aca tengo que armar el redirect al puerto correspondiente
      res.writeHead(200, { 'Content-Type': 'application/json' });
      req.on('data', async (data) =>
        {
          try 
          {
            var jsdata = JSON.parse(data);
            var custConfig = require('./.config/'+jsdata.from.toString().split("@")[0]+'.js');
            logear("redirect =>", custConfig.httpPort)
            await sendPost(http, 'localhost', custConfig.httpPort, req.url, data, (ret) => {
              res.end(JSON.stringify(ret));
            });
          } 
          catch (e) 
          {
            logear(e);
            res.writeHead(500);
            res.end(JSON.stringify({ res:'ERR' }) );
          }
        }
      );
    }
    else if( req.method == 'POST' && !esQR ) 
    {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        req.on('data', async (data) =>
          {
            try 
            {
              var jsdata = JSON.parse(data);
              logear(req.url, jsdata)
              switch (req.url) 
              {
                case "/sendGroupInvitation":
                    jsdata.to = (jsdata.to.toString().match('@')) ? jsdata.to : jsdata.to+'@c.us';
                    jsdata.group = (jsdata.group.toString().match('@')) ? jsdata.group : jsdata.group+'@g.us';
/*                    var gchat = await client.getChatById('120363164928457301@g.us');
                    logear("gchat", gchat);
                    var result = await gchat.addParticipants([jsdata.to], {comment: jsdata.message} );
                    logear(result);
*/
                    const result = await client.sendGroupV4Invite(jsdata.to, jsdata.group, jsdata.message);
                    logear(result, jsdata);
                    res.end( JSON.stringify({ res:'OK', msg: result }));
                  break;
                case "/sendMessage":
                  // aca meto parche por que no anda ubialertas. capaz que pueda quedar igual, poer que voy a hacer es enviar el codigo de activacion para que se pueda ver desde el despacho
                  if (whatsappid=="5491128419699" && (jsdata.message.substring(0,21)=="Codigo de activacion:" || jsdata.message.substring(0,21)=="Código de activación:")){
                    sendWebHook({event: 'activation_code', content: jsdata});
                  }
                  var msg;
                  if (jsdata.message.substring(0,4)!='http')
                  {
                    msg = jsdata.message
                  } 
                  else 
                  {
                    if (jsdata.message.substring(0,23)=='https://maps.google.com') 
                    {
                      var loc = parseGoogleMapLocation(jsdata.message);
                      msg = new Location(loc.lat, loc.lon, '');
                    } 
                    else 
                    {
                      msg = await MessageMedia.fromUrl(jsdata.message);
                      msg.mimetype = 'audio/ogg; codecs=opus';
                    }
                  }
                  jsdata.to = (jsdata.to.toString().match('@')) ? jsdata.to : jsdata.to+'@c.us';
                  var newmsg = await sendMessage(jsdata.to, msg, { sendAudioAsVoice: true }  );
                  //logear(newmsg);
                  res.end( JSON.stringify({ res:'OK', msg: newmsg }));
                  break;
                case "/getMessage":
                  var msg = await client.getMessageById(jsdata.msgId);
                  res.end( JSON.stringify({ res:'OK', msg: msg }));
                  break;
                case "/getChat":
                  var msg = await client.getChatById(jsdata.chatId);
                  res.end( JSON.stringify({ res:'OK', msg: msg }));
                  break;
                case "/checkState":
                  await client.resetState();
                  res.end( JSON.stringify({ res:'OK', msg: '' }));
                  break;
                case "/disconnect":
                  logear("(disconnect) Cerrando la session");
                  await client.logout();
                  haySession=false;
                  await cerrarSession(true);
                  res.end( JSON.stringify({ res:'OK', msg: '' }));
                  process.exit();  
                  break;
                case "/reconnect":
                  logear("(reconnect) Reconectando");
                  await cerrarSession(false);
                  res.end( JSON.stringify({ res:'OK', msg: '' }));
                  process.exit();  
                  break;
                default:
                  res.writeHead(404);
                  res.end(JSON.stringify({ res:'ERR' }) );
              }
            } 
            catch (e) 
            {
              logear(e);
              res.writeHead(500);
              res.end(JSON.stringify({ res:'ERR' }) );
            }
          }
        );
    } else {
      res.writeHead(500);
      res.end(JSON.stringify({ res:'ERR' }) );
    }
  } catch (e) 
  {
    logear(e);
    res.writeHead(500);
    res.end(JSON.stringify({ res:'ERR' }) );
  }
}
client.on('qr', async (qr) => 
  {
    try
    {
      if (haySession) 
      {
        logear('SESSION NO ES VALIDA, Y ACA NO PODEMOS LEER LOS QR, ELIMINO Y ME VOY');
        await cerrarSession(true);
        process.exit();
        return;
      }
      // NOTE: This event will not be fired if a session is specified.
      //logear('QR RECEIVED', qr);
      //qrcode.generate(qr);
      logear("nuevo qr");
      cantQR++;
      if (cantQR<5)
      {
        qrcode.toString(qr, {type:'terminal', small:true},(err, code) =>
          {
            if(err) return logear("error occurred")
            logear('\n\n'+code)
          }
        );
        qrcode.toDataURL(qr, (err, code) =>
          {
            if(err) return logear("error occurred")
            imgQR = code;
          }
        );
      } 
      else 
      {
        imgQR='';
        cantQR=-1;
        initializing=false;
        logear('NO SE ESCANEO QR, ELIMINO Y ME VOY');
        await cerrarSession(true);
        process.exit();
      }
    }
    catch(e)
    {
      logear(e);
    }
  }
);
client.on('authenticated', async (session) => 
  {
    try 
    {
      haySession=true;
      logear('AUTHENTICATED', session);
      var WWebVersion = await client.getWWebVersion();
      logear("WWebVersion:", WWebVersion);
      sessionCfg=session;
      imgQR='Conectando'; //la vacio para que no puedan seguir escaneando
      //sendWebHook({event: 'authenticated', content: session});
    }
    catch(e)
    {
      logear(e);
    }
  }
);
client.on('auth_failure', async (msg) => 
  {
    try 
    {
      // Fired if session restore was unsuccessfull
      logear('AUTHENTICATION_FAILURE', msg);
      await cerrarSession(true);
      process.exit();
    }
    catch(e)
    {
      logear(e);
      await cerrarSession(true);
}
  }
);
client.on('ready', async () => 
  {
    try
    {
      logear('READY', whatsappid);
      logear(client.info);
      if (client.info.wid.user != whatsappid)
      {
        logear("SE CONECTO UN NUMERO DISTINTO")
        sleep(2000).then(async ()=>
          {
            logear("Desconecto la sesion");
            await client.logout();
            haySession=false;
            await cerrarSession(true);
            process.exit();
          })
      }
      else
      {
        sendWebHook({event: 'change_state', content: 'CONNECTED'});
        //deleteChats();
      }
    }
    catch(e)
    {
      logear(e);
    }
  }
);
/*client.on('message', async msg => {
    //logear('MESSAGE RECEIVED', msg);
    //este no lo mando, por que en message_create va todo, lo que llega y lo que se manda
    //sendWebHook({event: 'message', content: msg});
});*/
client.on('message_create', async (msg) => 
  {
    try {
      // Fired on all message creations, including your own
      logear('message_create', JSON.stringify(msg));
      var event = 'message_recived';
      if (msg.from == 'status@broadcast' || msg.type == "e2e_notification")
      {
        event = false;
      } 
      else 
      {
        if (msg.type == 'ciphertext') 
        {
          //espero hasta que este descifrado
          var cont=0;
          while (msg.type == 'ciphertext' && cont<=50) 
          { 
            cont++;
            await sleep(500);
            msg = await client.getMessageById(msg.id._serialized);
            logear("ciphertext => ", msg.type, cont);
          }
        }
        if (msg.fromMe) 
        {
          var event = 'message_sent';
          /*if ( msg.type!='chat' ) 
          {
            event = false; //spero a que termine se ejecute media_uploaded
            lstMensajes.push(msg.id._serialized);
          }*/
        }
      }
      if (event) 
      {
        sendEventMessage(event, msg);
      }
      //refreshChats();
    }
    catch(e)
    {
      logear(e);
    }
  }
);
client.on('media_uploaded', (msg) => 
  {
    try
    {
      if (lstMensajes.indexOf(msg.id._serialized)>=0)
      {
        //logear ('media_uploaded', JSON.stringify(msg));
        if (
            (msg.type=='location' && (msg.location.latitude!=0 || msg.location.longitude!=0)) ||
            (msg.type!='chat' && msg.hasMedia )
          )
        {
          lstMensajes.splice(lstMensajes.indexOf(msg.id._serialized),1);
          sendEventMessage('message_sent', msg);
        }
      }
    }
    catch(e)
    {
      logear(e);
    }
  }
);
client.on('message_revoke_everyone', async (after, before) => 
  {
    try
    {
      // Fired whenever a message is deleted by anyone (including you)
      //logear(after); // message after it was deleted.
      sendWebHook({event: 'message_revoke_everyone', content: {after: after, before: before}});
      //if (before) {
      //    logear(before); // message before it was deleted.
      //}
    }
    catch(e)
    {
      logear(e);
    }
  }
);
/*client.on('message_revoke_me', async (msg) => {
    // Fired whenever a message is only deleted in your own view.
    //logear(msg); // message before it was deleted.
    //sendWebHook({event: 'message_revoke_me', content: msg});
});*/
client.on('message_ack', (msg, ack) => 
  {
    try
    {
      //        == ACK VALUES ==
      //        ACK_ERROR: -1
      //        ACK_PENDING: 0
      //        ACK_SERVER: 1
      //        ACK_DEVICE: 2
      //        ACK_READ: 3
      //        ACK_PLAYED: 4
      //logear('MESSAGE_ACK', ack, msg);
      sendWebHook({event: 'message_ack', content: {msg:  msg, ack: ack}});
      //if(ack == 3) {
          // The message was read
      //}
    }
    catch(e)
    {
      logear(e);
    }
  }
);

client.on('group_join', async (notification) => {
    // User has joined or been added to the group.
    var chat = await client.getChatById(notification.chatId);
    logear('group_join', notification);
    logear('chat', chat);
    sendWebHook({event: 'group_join', content: {notification: notification, chat: chat}});
    //notification.reply('User joined.');
});
client.on('group_leave', async (notification) => {
    // User has left or been kicked from the group.
    var chat = await client.getChatById(notification.chatId);
    logear('group_leave', notification);
    logear('chat', chat);
    sendWebHook({event: 'group_leave', content: {notification: notification, chat: chat}});
    //notification.reply('User left.');
});
client.on('group_update', async (notification) => {
    // Group picture, subject or description has been updated.
    var chat = await client.getChatById(notification.chatId);
    logear('group_update', notification);
    logear('chat', chat);
    sendWebHook({event: 'group_update', content: {notification: notification, chat: chat}});
});

client.on('change_battery', (batteryInfo) => 
  {
    try
    {
      // Battery percentage for attached device has changed
      //const { battery, plugged } = batteryInfo;
      //logear(`Battery: ${battery}% - Charging? ${plugged}`);
      sendWebHook({event: 'change_battery', content: batteryInfo});
    }
    catch(e)
    {
      logear(e);
    }
  }
);
client.on('change_state', state => 
  {
    try
    {
      logear('CHANGE STATE', state );
      sendWebHook({event: 'change_state', content: state});
    }
    catch(e)
    {
      logear(e);
    }
  }
);
client.on('disconnected', async (reason) => 
  {
    try
    {
      logear('Client was logged out', reason);
      await cerrarSession(true);
      process.exit();
    }
    catch(e)
    {
      logear(e);
    }
  }
);

async function cerrarSession(eliminar)
{
  try
  {
    logear("cierro sesion", (haySession)?'SI':'NO', (initializing)?'SI':'NO');
    if (haySession || initializing) 
    {
      logear("cierro el cliente");
      await client.destroy();
      logear("se cerro el cliente");
    }
    if (!esQR) 
    {
      logear("No soy QR, aviso al webhook");
      await sendWebHook({event: 'change_state', content: 'DISCONNECTED'}, () =>
        {
          if (eliminar) 
          {
            logear("Elimino la sesion local");
            fs.rmSync('.wwebjs_auth/session-'+whatsappid,{ recursive: true, force: true });
          }
          logear("me voy");
        }
      );
    }
    else 
    {
      logear("Soy QR, me voy");
      process.exit();
    }    
  }
  catch(e)
  {
    logear(e);
  }
}
function logear(...args)
{
  try
  {
    fs.appendFile('/var/log/whatsapi/whatsapi.log', JSON.stringify([new Date().toISOString(),whatsappid, ...args]) + '\n', (err) =>
      {
        if (err) {
            console.log(err);
        }
      }
    );
    console.log(new Date().toISOString(), whatsappid, ...args);
  }
  catch(e)
  {
    console.error(e);
  }
}
function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
async function sendMessage (to, message, options = {}){
  try 
  {
    const chat = await client.getChatById(to);
    if (chat) {
      chat.sendStateTyping();
      await sleep(50);//*message.length());
      return client.sendMessage(to, message, options);
    }
  } 
  catch(e)
  {
    logear(e, to, message);
  }
}
async function sendEventMessage (event, msg, numretry=1){
  try 
  {
    var lineaOK = await tick(event);
    if (!lineaOK || numretry>10)
    {
      return;
    }
    logear(
        (msg.fromMe) ? msg.from : msg.to,
        (msg.fromMe) ? "=>" : "<=",
        (!msg.fromMe) ? msg.from : msg.to,
        msg.type,
        (msg.type=='chat' ? msg.body : '')
    );
    /*while (lstSending.indexOf(msg.from)>=0)
    {
      logear("espero que se libere", msg.from, msg.id._serialized, lstSending.indexOf(msg.from), lstSending);
      await sleep(500);
    }*/
    if (!msg.fromMe) 
    {
      lstSending.push(msg.from);
    }
    let attachmentData;
    if (msg.hasMedia)
    {
      attachmentData = await msg.downloadMedia(fs);
    }
    if (msg.hasQuotedMsg)
    {
      msg.quotedMsg = await msg.getQuotedMessage();
    }
    msg.chat = await client.getChatById((msg.fromMe) ? msg.to : msg.from);
    sendWebHook({event: event, content: msg, media: attachmentData}, (resp) => 
      {
        sended=true;
        lstSending.splice(lstSending.indexOf(msg.from),1);
        logear(
            (msg.fromMe) ? msg.from : msg.to,
            (msg.fromMe) ? "=>" : "<=",
            (!msg.fromMe) ? msg.from : msg.to,
            msg.type,
            (msg.type=='chat' ? msg.body : ''),
            "OK"+numretry
        );
        if (resp.autoreply) {
          sendMessage(msg.from, resp.autoreply);
        }
      }, (err) => 
      {
        logear(
          (msg.fromMe) ? msg.from : msg.to,
          (msg.fromMe) ? "=>" : "<=",
          (!msg.fromMe) ? msg.from : msg.to,
          msg.type,
          (msg.type=='chat' ? msg.body : ''),
          err
        );
        sleep(1000);
        logear(
          (msg.fromMe) ? msg.from : msg.to,
          (msg.fromMe) ? "=>" : "<=",
          (!msg.fromMe) ? msg.from : msg.to,
          msg.type,
          (msg.type=='chat' ? msg.body : ''),
          "RETRY-"+numretry
        );
        numretry++;
        sendEventMessage (event, msg, numretry);
    });
  } 
  catch(e)
  {
    logear(e);
    lstSending.splice(lstSending.indexOf(msg.from),1);
  }
}
async function tick(event)
{
  //aca tenemos que enviar a la api para que inserte en lineastick
  //y nos devuelva el estado de la linea, 
  // => event, whatsappid
  // si vuelve que hay que alertar, enviamos al webhook a alerta
      //sendWebHook({event: 'event', content: msg, media: attachmentData}, (resp) => {
      //  });
      //}
  return true;
}

async function delete1Msg(chat)
{
  try 
  {
    logear("Eliminando 1Msg en ", chat.id._serialized);
    var ahora = new Date().getTime();
    var horadelete = new Date(ahora-(1000*60*60*48)).getTime();
    g_chats[chat.id._serialized] = ahora;
    var msgs = await chat.fetchMessages({limit: 9999});
    logear("MSGS:", msgs);
//    chat.fetchMessages({limit: 9999}).then(async (msgs) => 
//      {
//        try 
//        {
          if (msgs.length==0)
          {
            logear("El chat esta vacio, lo elimino:", chat.id._serialized);
            chat.delete().then(
              (res) => 
              {
                logear("Chat eliminado:", chat.id._serialized, res);
              }
            )
          } 
          else 
          {
            //var msg = msgs[0];
            for (var idmsg in msgs){
              var msg = msgs[idmsg];
              var msgtime = msg.timestamp*1000;
              logear("MSG:", msg);
              logear("Revisando Mensaje ", msg.id._serialized, msgtime, horadelete, msgtime<=horadelete);
              if (msgtime<=horadelete) 
              {
                logear("Eliminando Mensaje ", msg.id._serialized);
                await msg.delete();
                await sleep(1000);                
                //setTimeout(delete1Msg, 1111, chat);
              } 
              else 
              {
                logear("No tengo mas para borrar aca", chat.id._serialized);
                break;
              }  
            }
          }
//        } 
//        catch (e) 
//        {
//          logear(e);
//        }
//      }
//    );
  } 
  catch (e) 
  {
    logear(e);
  }
}

async function deleteChats()
{
  try 
  {
    logear("Eliminando Chats");
    const chats = await client.getChats();
    logear(chats);
    var ahora = new Date().getTime();
    var _1hora = new Date(ahora-(1000*60*60)).getTime();
    for (const chat of chats) 
    {
      logear("Chat: ", chat.id._serialized);
      if (!g_chats[chat.id._serialized] || (g_chats[chat.id._serialized] && g_chats[chat.id._serialized]<=_1hora))
      {
        logear("Busco para borrar: ", chat.id._serialized);
        await delete1Msg(chat);
      }
      await sleep(1000);
    };
  } 
  catch (e) 
  {
    logear(e);
  }
  setTimeout(deleteChats, (1000*60*60));
}

/*
async function refreshChats(){
  try {
    const chats = await client.getChats();
    if (g_chats!=null){
      chats.forEach( (chat) => {
          const f_chat = g_chats.find( (o) => {
              return (o.id._serialized === chat.id._serialized) && (o.pinned !== chat.pinned || o.isMuted !== chat.isMuted)
          });
          if (f_chat && f_chat != chat) {
              sendWebHook({event: 'chat_updated', content: chat});
          }
      });
    }
    g_chats = chats;
  } catch (e) {
    logear(e);
  }
}
*/
async function sendWebHook(datas, callback = null, callerr = null)
{
  try
  {
    if ( datas.content.type && (datas.content.type=='notification_template' || datas.content.type=='call_log' || datas.content.type=='protocol') ) 
    {
      logear('No lo mando', datas);
    } else {
      datas.whatsappid = whatsappid;
      var postData = 'data='+urlencode.encode(JSON.stringify(datas));
      logear(postData);
      await sendPost( (config.webhook_protocol=='https') ? https : http , config.webhook_host, config.webhook_port, config.webhook_url, postData, callback, callerr);
    }
  }
  catch(e)
  {
    logear(e);
  }
}
async function sendGet(protocol, host, port, url, callback = null, callerr = null)
{
	try
  {
	    //var authorization = "Basic " + new Buffer.from("USERNAME:PASSWORD").toString("base64");
    var options = 
    {
      host: host,
      port: port,
      path: url,
      method: 'GET',
    };

    var reqp = protocol.request(options, (resp) =>
      {
        try
        {
          resp.setEncoding('utf8');
          resp.on('data', (chunk) =>
            {
              try
              {
                var resdata = chunk;
                if (callback) callback(resdata);
              }
              catch(e)
              {
                if (callerr) 
                {
                  callerr(e);
                } else 
                {
                  logear(e);
                }
                var resdata = JSON.parse('[{"message":"Data not Found"}]');
                //logear(resdata);
              }
            }
          );
        }
        catch(e)
        {
          logear(e);
        }    
      }
    );
    reqp.on('error', (e) =>
      {
        if (callerr) {
          callerr(e);
        } else {
            logear('problem with request: ' + e.message);
        }
      }
    );
    // write data to request body
    //reqp.write(postData);
    reqp.end();
	}
  catch(e)
  {
    if (callerr)
    {
      callerr(e);
    } 
    else 
    {
	    logear(e);
    }
	}
}
async function sendPost(protocol, host, port, url, postData, callback = null, callerr = null)
{
  var espero=true;
	try
  {
    var authorization = "Basic " + new Buffer.from("USERNAME:PASSWORD").toString("base64");
    var options = 
    {
      host: host,
      port: port,
      path: url,
      method: 'POST',
      headers : {
          "Authorization" : authorization,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(postData)
      }
    };

    var reqp = protocol.request(options, (resp) =>
      {
        try
        {
          resp.setEncoding('utf8');
          resp.on('data', (chunk) =>
            {
              espero=false;
              try
              {
                var resdata = JSON.parse(chunk);
                if (callback) callback(resdata);
              }
              catch(e)
              {
                if (callerr) 
                {
                  callerr(e);
                } 
                else 
                {
                  logear(e);
                }
                var resdata = JSON.parse('[{"message":"Data not Found"}]');
                //logear(resdata);
              }
            }
          );
        }
        catch(e)
        {
          logear(e);
        }
      }
    );
    reqp.on('error', (e)  =>
      {
        espero=false;
        if (callerr) {
          callerr(e);
        } else {
            logear('problem with request: ' + e.message);
        }
      }
    );
    // write data to request body
    reqp.write(postData);
    reqp.end();
	}
  catch(e)
  {
    if (callerr)
    {
      callerr(e);
    } 
    else 
    {
	    logear(e);
    }
	}  
  while (espero) {
    await sleep(100);
  }
}
function parseGoogleMapLocation(url)
{
  try
  {
    var lonLatStr = url.split('=')[1];
    lonlat = lonLatStr.split(',');
    return {lon: lonlat[1], lat: lonlat[0]}
  }
  catch(e)
  {
    logear(e);
  }
}
